import { Module } from '@nestjs/common';
import { ConfigServiceSettingsModule } from 'frameworks/settings/config-service/config-service-settings.module';

@Module({
  imports: [ConfigServiceSettingsModule],
  exports: [ConfigServiceSettingsModule],
})
export class SettingsModule {}
