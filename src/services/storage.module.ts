import { Module } from '@nestjs/common';
import { LocalStorageModule } from 'frameworks/storage/local/local-storage.module';

@Module({
  imports: [LocalStorageModule],
  exports: [LocalStorageModule],
})
export class StorageModule {}
