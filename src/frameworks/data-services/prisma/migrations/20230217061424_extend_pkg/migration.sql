/*
  Warnings:

  - Added the required column `repoId` to the `Pkg` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Pkg" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "repoId" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    CONSTRAINT "Pkg_repoId_fkey" FOREIGN KEY ("repoId") REFERENCES "Repo" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Pkg" ("id", "name") SELECT "id", "name" FROM "Pkg";
DROP TABLE "Pkg";
ALTER TABLE "new_Pkg" RENAME TO "Pkg";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
