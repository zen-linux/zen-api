/*
  Warnings:

  - Added the required column `desc` to the `Pkg` table without a default value. This is not possible if the table is not empty.
  - Added the required column `rel` to the `Pkg` table without a default value. This is not possible if the table is not empty.
  - Added the required column `size` to the `Pkg` table without a default value. This is not possible if the table is not empty.
  - Added the required column `ver` to the `Pkg` table without a default value. This is not possible if the table is not empty.

*/
-- CreateTable
CREATE TABLE "_PkgDeps" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    CONSTRAINT "_PkgDeps_A_fkey" FOREIGN KEY ("A") REFERENCES "Pkg" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "_PkgDeps_B_fkey" FOREIGN KEY ("B") REFERENCES "Pkg" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Pkg" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "repoId" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "desc" TEXT NOT NULL,
    "ver" TEXT NOT NULL,
    "rel" INTEGER NOT NULL,
    "size" INTEGER NOT NULL,
    CONSTRAINT "Pkg_repoId_fkey" FOREIGN KEY ("repoId") REFERENCES "Repo" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Pkg" ("id", "name", "repoId") SELECT "id", "name", "repoId" FROM "Pkg";
DROP TABLE "Pkg";
ALTER TABLE "new_Pkg" RENAME TO "Pkg";
CREATE UNIQUE INDEX "Pkg_repoId_name_ver_rel_key" ON "Pkg"("repoId", "name", "ver", "rel");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;

-- CreateIndex
CREATE UNIQUE INDEX "_PkgDeps_AB_unique" ON "_PkgDeps"("A", "B");

-- CreateIndex
CREATE INDEX "_PkgDeps_B_index" ON "_PkgDeps"("B");
