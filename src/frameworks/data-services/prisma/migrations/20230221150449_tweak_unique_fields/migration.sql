/*
  Warnings:

  - A unique constraint covering the columns `[repoId,name]` on the table `Pkg` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[groupId,name]` on the table `Repo` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "Repo_name_key";

-- CreateIndex
CREATE UNIQUE INDEX "Pkg_repoId_name_key" ON "Pkg"("repoId", "name");

-- CreateIndex
CREATE UNIQUE INDEX "Repo_groupId_name_key" ON "Repo"("groupId", "name");
