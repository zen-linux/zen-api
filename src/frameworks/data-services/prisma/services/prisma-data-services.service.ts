import { Injectable } from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { PrismaApiKeyRepository } from '../repositories/prisma-api-key-repository';
import { PrismaGroupRepository } from '../repositories/prisma-group-repository';
import { PrismaMemberRepository } from '../repositories/prisma-member-repository';
import { PrismaPkgRepository } from '../repositories/prisma-pkg-repository';
import { PrismaRepoRepository } from '../repositories/prisma-repo-repository';
import { PrismaUserRepository } from '../repositories/prisma-user-repository';

@Injectable()
export class PrismaDataServices implements DataServices {
  constructor(
    public apikeys: PrismaApiKeyRepository,
    public groups: PrismaGroupRepository,
    public members: PrismaMemberRepository,
    public pkgs: PrismaPkgRepository,
    public repos: PrismaRepoRepository,
    public users: PrismaUserRepository,
  ) {}
}
