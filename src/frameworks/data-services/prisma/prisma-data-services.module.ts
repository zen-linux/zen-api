import { Module } from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { PrismaApiKeyRepository } from './repositories/prisma-api-key-repository';
import { PrismaGroupRepository } from './repositories/prisma-group-repository';
import { PrismaMemberRepository } from './repositories/prisma-member-repository';
import { PrismaPkgRepository } from './repositories/prisma-pkg-repository';
import { PrismaRepoRepository } from './repositories/prisma-repo-repository';
import { PrismaUserRepository } from './repositories/prisma-user-repository';
import { PrismaDataServices } from './services/prisma-data-services.service';
import { PrismaService } from './services/prisma.service';

@Module({
  imports: [],
  providers: [
    PrismaService,
    {
      provide: DataServices,
      useClass: PrismaDataServices,
    },
    PrismaApiKeyRepository,
    PrismaGroupRepository,
    PrismaMemberRepository,
    PrismaPkgRepository,
    PrismaRepoRepository,
    PrismaUserRepository,
  ],
  exports: [DataServices],
})
export class PrismaDataServicesModule {}
