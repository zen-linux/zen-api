import { Injectable, NotFoundException } from '@nestjs/common';
import { MemberRepository } from 'core/abstracts/repositories/member-repository';
import { Member } from 'core/entities/member';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class PrismaMemberRepository implements MemberRepository {
  constructor(private prisma: PrismaService) {}

  getAll(): Promise<Member[]> {
    return this.prisma.member.findMany({
      include: { group: true, user: true },
    });
  }

  getAllForGroupId(groupId: EntityId): Promise<Member[]> {
    return this.prisma.member.findMany({
      include: { group: true, user: true },
      where: { groupId },
    });
  }

  async get(id: EntityId): Promise<Member> {
    const member = await this.prisma.member.findUnique({
      include: { group: true, user: true },
      where: { id },
    });
    if (!member) {
      throw new NotFoundException();
    }
    return member;
  }

  async create(item: Member): Promise<Member> {
    return this.prisma.member.create({
      include: { group: true, user: true },
      data: {
        isAdmin: item.isAdmin,
        group: {
          connect: {
            id: item.group.id,
          },
        },
        user: {
          connect: {
            id: item.user.id,
          },
        },
      },
    });
  }

  async update(id: EntityId, item: Member): Promise<Member> {
    return this.prisma.member.update({
      where: { id },
      include: { group: true, user: true },
      data: {
        isAdmin: item.isAdmin,
        group: {
          connect: {
            id: item.group.id,
          },
        },
        user: {
          connect: {
            id: item.user.id,
          },
        },
      },
    });
  }

  async delete(id: EntityId): Promise<void> {
    /*const member = await this.prisma.member.findFirst({ where: { id } });
    if (!member) {
      throw new NotFoundException();
    }*/
    await this.prisma.member.delete({ where: { id } }); // TODO check behaviour when id does not exist
  }

  async deleteAll(): Promise<void> {
    await this.prisma.member.deleteMany();
  }
}
