import { Injectable, NotFoundException } from '@nestjs/common';
import { UserRepository } from 'core/abstracts/repositories/user-repository';
import { User } from 'core/entities/user';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class PrismaUserRepository implements UserRepository {
  constructor(private prisma: PrismaService) {}

  getAll(): Promise<User[]> {
    return this.prisma.user.findMany();
  }

  async get(id: EntityId): Promise<User> {
    const user = await this.prisma.user.findUnique({ where: { id } });
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async getByEmail(email: string): Promise<User> {
    const user = await this.prisma.user.findUnique({ where: { email } });
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async create(item: User): Promise<User> {
    return this.prisma.user.create({ data: item });
  }

  async update(id: EntityId, item: User): Promise<User> {
    /*const user = await this.userModel.findOne({ where: { id } });
    if (!user) {
      throw new NotFoundException();
    }*/
    return this.prisma.user.update({ where: { id }, data: item }); // TODO check behaviour when id does not exist
  }

  async delete(id: EntityId): Promise<void> {
    /*const user = await this.prisma.user.findFirst({ where: { id } });
    if (!user) {
      throw new NotFoundException();
    }*/
    await this.prisma.user.delete({ where: { id } }); // TODO check behaviour when id does not exist
  }

  async deleteAll(): Promise<void> {
    await this.prisma.user.deleteMany();
  }
}
