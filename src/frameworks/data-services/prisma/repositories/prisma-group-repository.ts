import { Injectable, NotFoundException } from '@nestjs/common';
import { GroupRepository } from 'core/abstracts/repositories/group-repository';
import { Group } from 'core/entities/group';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class PrismaGroupRepository implements GroupRepository {
  constructor(private prisma: PrismaService) {}

  getAll(): Promise<Group[]> {
    return this.prisma.group.findMany();
  }

  async get(id: EntityId): Promise<Group> {
    const group = await this.prisma.group.findUnique({ where: { id } });
    if (!group) {
      throw new NotFoundException();
    }
    return group;
  }

  async getByIdForUserId(groupId: EntityId, userId: EntityId): Promise<Group> {
    const group = await this.prisma.group.findUnique({
      include: { Member: { where: { userId } } },
      where: { id: groupId },
    });
    if (!group) {
      throw new NotFoundException();
    }
    return group;
  }

  async getByName(name: EntityId): Promise<Group> {
    const group = await this.prisma.group.findFirst({ where: { name } });
    if (!group) {
      throw new NotFoundException();
    }
    return group;
  }

  async create(item: Group): Promise<Group> {
    return this.prisma.group.create({ data: item });
  }

  async update(id: EntityId, item: Group): Promise<Group> {
    /*const group = await this.groupModel.findOne({ where: { id } });
    if (!group) {
      throw new NotFoundException();
    }*/
    return this.prisma.group.update({ where: { id }, data: item }); // TODO check behaviour when id does not exist
  }

  async delete(id: EntityId): Promise<void> {
    /*const group = await this.prisma.group.findFirst({ where: { id } });
    if (!group) {
      throw new NotFoundException();
    }*/
    await this.prisma.group.delete({ where: { id } }); // TODO check behaviour when id does not exist
  }

  async deleteAll(): Promise<void> {
    await this.prisma.group.deleteMany();
  }
}
