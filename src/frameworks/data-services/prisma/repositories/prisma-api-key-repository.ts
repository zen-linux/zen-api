import { Injectable, NotFoundException } from '@nestjs/common';
import { ApiKeyRepository } from 'core/abstracts/repositories/api-key-repository';
import { ApiKey } from 'core/entities/api-key';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class PrismaApiKeyRepository implements ApiKeyRepository {
  constructor(private prisma: PrismaService) {}

  getAll(): Promise<ApiKey[]> {
    return this.prisma.apiKey.findMany({ include: { user: true } });
  }

  async getAllByUserId(userId: EntityId): Promise<ApiKey[]> {
    return await this.prisma.apiKey.findMany({
      include: { user: true },
      where: { userId },
    });
  }

  async get(id: EntityId): Promise<ApiKey> {
    const apiKey = await this.prisma.apiKey.findUnique({
      include: { user: true },
      where: { id },
    });
    if (!apiKey) {
      throw new NotFoundException();
    }
    return apiKey;
  }

  async getByKey(key: string): Promise<ApiKey> {
    const apiKey = await this.prisma.apiKey.findUnique({
      include: { user: true },
      where: { key },
    });
    if (!apiKey) {
      throw new NotFoundException();
    }
    return apiKey;
  }

  async create(item: ApiKey): Promise<ApiKey> {
    return this.prisma.apiKey.create({
      include: { user: true },
      data: {
        key: item.key,
        description: item.description,
        user: {
          connect: {
            id: item.user.id,
          },
        },
      },
    });
  }

  async update(id: EntityId, item: ApiKey): Promise<ApiKey> {
    return this.prisma.apiKey.update({
      include: { user: true },
      where: { id },
      data: {
        key: item.key,
        description: item.description,
        user: {
          connect: {
            id: item.user.id,
          },
        },
      },
    });
  }

  async delete(id: EntityId): Promise<void> {
    await this.prisma.apiKey.delete({ where: { id } });
  }

  async deleteByIdForUserId(id: string, userId: string): Promise<void> {
    await this.prisma.apiKey.delete({ where: { id } });
  }

  async deleteAll(): Promise<void> {
    await this.prisma.apiKey.deleteMany();
  }

  async deleteAllForUserId(userId: string): Promise<void> {
    await this.prisma.apiKey.deleteMany({ where: { userId } });
  }
}
