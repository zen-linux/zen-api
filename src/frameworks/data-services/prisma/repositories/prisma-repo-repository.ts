import { Injectable, NotFoundException } from '@nestjs/common';
import { RepoRepository } from 'core/abstracts/repositories/repo-repository';
import { Repo } from 'core/entities/repo';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class PrismaRepoRepository implements RepoRepository {
  constructor(private prisma: PrismaService) {}

  getAll(): Promise<Repo[]> {
    return this.prisma.repo.findMany({ include: { group: true } });
  }

  getAllForUserId(userId: string): Promise<Repo[]> {
    return this.prisma.repo.findMany({
      include: { group: true },
      where: { group: { Member: { some: { userId } } } },
    });
  }

  getAllByGroupId(groupId: string): Promise<Repo[]> {
    return this.prisma.repo.findMany({
      include: { group: true },
      where: { groupId },
    });
  }

  async get(id: EntityId): Promise<Repo> {
    const repo = await this.prisma.repo.findUnique({
      include: { group: true },
      where: { id },
    });
    if (!repo) {
      throw new NotFoundException();
    }
    return repo;
  }

  async getByIdForUserId(repoId: EntityId, userId: EntityId): Promise<Repo> {
    const repo = await this.prisma.repo.findUnique({
      include: { group: { include: { Member: { where: { userId } } } } },
      where: { id: repoId },
    });
    if (!repo) {
      throw new NotFoundException();
    }
    return repo;
  }

  async create(item: Repo): Promise<Repo> {
    return this.prisma.repo.create({
      include: { group: true },
      data: {
        name: item.name,
        group: {
          connect: { id: item.group.id },
        },
      },
    });
  }

  async update(id: EntityId, item: Repo): Promise<Repo> {
    /*const repo = await this.repoModel.findOne({ where: { id } });
    if (!repo) {
      throw new NotFoundException();
    }*/
    return this.prisma.repo.update({
      include: { group: true },
      where: { id },
      data: {
        name: item.name,
        group: { connect: { id: item.group.id } },
      },
    }); // TODO check behaviour when id does not exist
  }

  async delete(id: EntityId): Promise<void> {
    /*const repo = await this.prisma.repo.findFirst({ where: { id } });
    if (!repo) {
      throw new NotFoundException();
    }*/
    await this.prisma.repo.delete({ where: { id } }); // TODO check behaviour when id does not exist
  }

  async deleteAll(): Promise<void> {
    await this.prisma.repo.deleteMany();
  }
}
