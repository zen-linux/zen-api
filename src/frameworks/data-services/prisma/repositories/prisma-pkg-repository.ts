import { Injectable, NotFoundException } from '@nestjs/common';
import { PkgRepository } from 'core/abstracts/repositories/pkg-repository';
import { Pkg } from 'core/entities/pkg';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class PrismaPkgRepository implements PkgRepository {
  constructor(private prisma: PrismaService) {}

  getAll(): Promise<Pkg[]> {
    return this.prisma.pkg.findMany({
      include: { repo: { include: { group: true } } },
    });
  }

  getAllByRepoId(repoId: EntityId): Promise<Pkg[]> {
    return this.prisma.pkg.findMany({
      include: { repo: { include: { group: true } } },
      where: { repoId },
    });
  }

  async get(id: EntityId): Promise<Pkg> {
    const pkg = await this.prisma.pkg.findUnique({
      include: {
        repo: { include: { group: true } },
      },
      where: { id },
    });
    if (!pkg) {
      throw new NotFoundException();
    }
    return pkg;
  }

  async create(item: Pkg): Promise<Pkg> {
    return this.prisma.pkg.create({
      include: { repo: { include: { group: true } } },
      data: {
        name: item.name,
        desc: item.desc,
        ver: item.ver,
        rel: item.rel,
        size: item.size,
        repo: {
          connect: {
            id: item.repo.id,
          },
        },
      },
    });
  }

  async update(id: EntityId, item: Pkg): Promise<Pkg> {
    /*const pkg = await this.pkgModel.findOne({ where: { id } });
    if (!pkg) {
      throw new NotFoundException();
    }*/
    return this.prisma.pkg.update({
      include: { repo: { include: { group: true } } },
      where: { id },
      data: {
        name: item.name,
        desc: item.desc,
        ver: item.ver,
        rel: item.rel,
        size: item.size,
        repo: {
          connect: {
            id: item.repo.id,
          },
        },
      },
    }); // TODO check behaviour when id does not exist
  }

  async delete(id: EntityId): Promise<void> {
    /*const pkg = await this.prisma.pkg.findFirst({ where: { id } });
    if (!pkg) {
      throw new NotFoundException();
    }*/
    await this.prisma.pkg.delete({ where: { id } }); // TODO check behaviour when id does not exist
  }

  async deleteForUserId(id: EntityId, userId: EntityId): Promise<void> {
    await this.prisma.pkg.delete({
      include: {
        repo: {
          include: { group: { include: { Member: { where: { userId } } } } },
        },
      },
      where: { id },
    });
  }

  async deleteAll(): Promise<void> {
    await this.prisma.pkg.deleteMany();
  }
}
