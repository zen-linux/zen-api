import { Module } from '@nestjs/common';
import { Storage } from 'core/abstracts/storage.abstract';
import { LocalStorage } from './local-storage.service';

@Module({
  providers: [
    {
      provide: Storage,
      useClass: LocalStorage,
    },
  ],
  exports: [Storage],
})
export class LocalStorageModule {}
