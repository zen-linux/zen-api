import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Settings } from 'core/abstracts/settings.abstract';
import { EnvironmentVariables } from './environment-variables';

@Injectable()
export class ConfigServiceSettings implements Settings {
  constructor(
    private configService: ConfigService<EnvironmentVariables, true>,
  ) {}

  getDataPath(): string {
    return this.configService.get('DATA_PATH');
  }

  getDbConnectionString(): string {
    return this.configService.get('DB_CONNECTION_STRING');
  }

  getJwtSecret(): string {
    return this.configService.get('JWT_SECRET');
  }
}
