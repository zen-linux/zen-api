import { plainToInstance } from 'class-transformer';
import { IsString, MinLength, validateSync } from 'class-validator';

export class EnvironmentVariables {
  @IsString()
  DB_CONNECTION_STRING!: string;

  @IsString()
  @MinLength(32)
  JWT_SECRET!: string;

  @IsString()
  DATA_PATH!: string;
}

export function validate(config: Record<string, any>): EnvironmentVariables {
  const validatedConfig = plainToInstance(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}
