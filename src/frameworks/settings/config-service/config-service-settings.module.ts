import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Settings } from 'core/abstracts/settings.abstract';
import { ConfigServiceSettings } from './config-service-settings.service';
import { validate } from './environment-variables';

@Module({
  imports: [
    ConfigModule.forRoot({
      validate,
    }),
  ],
  providers: [
    {
      provide: Settings,
      useClass: ConfigServiceSettings,
    },
  ],
  exports: [Settings],
})
export class ConfigServiceSettingsModule {}
