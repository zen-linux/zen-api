import { Injectable } from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { CreateGroupDto, UpdateGroupDto } from 'core/dtos/group.dto';
import { Group } from 'core/entities/group';
import { Member } from 'core/entities/member';
import { GroupFactoryService } from './group-factory.service';

@Injectable()
export class GroupUseCases {
  constructor(
    private dataServices: DataServices,
    private groupFactoryService: GroupFactoryService,
  ) {}

  getAllGroups(): Promise<Group[]> {
    return this.dataServices.groups.getAll();
  }

  getGroupById(id: EntityId): Promise<Group | null> {
    return this.dataServices.groups.get(id);
  }

  getGroupByName(name: string): Promise<Group> {
    return this.dataServices.groups.getByName(name);
  }

  async createGroup(
    userId: EntityId,
    createGroupDto: CreateGroupDto,
  ): Promise<Group> {
    const user = await this.dataServices.users.get(userId);

    const group = await this.dataServices.groups.create(
      await this.groupFactoryService.create(createGroupDto),
    );

    await this.dataServices.members.create(
      new Member({
        isAdmin: true,
        group,
        user,
      }),
    );

    return group;
  }

  async updateGroup(
    id: EntityId,
    updateGroupDto: UpdateGroupDto,
  ): Promise<Group> {
    const group = await this.groupFactoryService.update(id, updateGroupDto);
    return this.dataServices.groups.update(id, group);
  }

  deleteGroup(id: EntityId) {
    return this.dataServices.groups.delete(id);
  }

  deleteAllGroups() {
    return this.dataServices.groups.deleteAll();
  }
}
