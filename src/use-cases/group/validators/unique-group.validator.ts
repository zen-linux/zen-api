import {
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';

import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { GroupUseCases } from 'use-cases/group/group-use-cases.service';

@ValidatorConstraint({ name: 'unique-group', async: true })
@Injectable()
export class UniqueGroupValidator implements ValidatorConstraintInterface {
  constructor(private readonly groupUseCases: GroupUseCases) {}

  async validate(groupName: string): Promise<boolean> {
    try {
      await this.groupUseCases.getGroupByName(groupName);
      throw new UnprocessableEntityException(
        'A group with this name already exists',
      );
    } catch (error: any) {
      if (error instanceof NotFoundException) {
        return true;
      }
      throw error;
    }
  }
}
