import { Module } from '@nestjs/common';
import { DataServicesModule } from 'services/data-services.module';
import { GroupFactoryService } from './group-factory.service';
import { GroupUseCases } from './group-use-cases.service';
import { UniqueGroupValidator } from './validators/unique-group.validator';

@Module({
  imports: [DataServicesModule],
  providers: [GroupFactoryService, GroupUseCases, UniqueGroupValidator],
  exports: [GroupFactoryService, GroupUseCases],
})
export class GroupUseCasesModule {}
