import { Injectable } from '@nestjs/common';
import { CreateGroupDto, UpdateGroupDto } from 'core/dtos/group.dto';
import { Group } from 'core/entities/group';
import { FactoryService } from 'core/services/factory-service';

@Injectable()
export class GroupFactoryService extends FactoryService<Group> {
  async create(createDto: CreateGroupDto): Promise<Group> {
    const group = new Group({
      name: createDto.name,
      description: createDto.description || '',
    });
    return group;
  }

  async update(id: EntityId, updateDto: UpdateGroupDto): Promise<Group> {
    const group = await this.dataServices.groups.get(id);

    if (updateDto.name !== undefined) {
      group.name = updateDto.name;
    }

    return group;
  }
}
