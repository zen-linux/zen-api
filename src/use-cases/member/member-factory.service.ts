import { Injectable } from '@nestjs/common';
import { CreateMemberDto, UpdateMemberDto } from 'core/dtos/member.dto';
import { Member } from 'core/entities/member';
import { FactoryService } from 'core/services/factory-service';

@Injectable()
export class MemberFactoryService extends FactoryService<Member> {
  async create(createMemberDto: CreateMemberDto): Promise<Member> {
    const group = await this.dataServices.groups.get(createMemberDto.groupId);
    const user = await this.dataServices.users.get(createMemberDto.userId);

    const member = new Member({
      isAdmin: createMemberDto.isAdmin,
      group: group,
      user: user,
    });

    return member;
  }

  async update(
    id: EntityId,
    updateMemberDto: UpdateMemberDto,
  ): Promise<Member> {
    const member = await this.dataServices.members.get(id);

    member.isAdmin = updateMemberDto.isAdmin;

    return member;
  }
}
