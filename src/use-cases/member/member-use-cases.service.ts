import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { CreateMemberDto, UpdateMemberDto } from 'core/dtos/member.dto';
import { Member } from 'core/entities/member';
import { MemberFactoryService } from './member-factory.service';

@Injectable()
export class MemberUseCases {
  constructor(
    private dataServices: DataServices,
    private memberFactoryService: MemberFactoryService,
  ) {}

  getAllMembers(): Promise<Member[]> {
    return this.dataServices.members.getAll();
  }

  getAllMembersForGroupId(groupId: EntityId): Promise<Member[]> {
    return this.dataServices.members.getAllForGroupId(groupId);
  }

  getMemberById(id: EntityId): Promise<Member | null> {
    return this.dataServices.members.get(id);
  }

  async createMember(
    groupId: EntityId,
    createMemberDto: CreateMemberDto,
  ): Promise<Member> {
    const member = await this.memberFactoryService.create(createMemberDto);
    return this.dataServices.members.create(member);
  }

  async updateMember(
    memberId: EntityId,
    updateMemberDto: UpdateMemberDto,
  ): Promise<Member> {
    const member = await this.memberFactoryService.update(
      memberId,
      updateMemberDto,
    );

    return this.dataServices.members.update(memberId, member);
  }

  async deleteMember(memberId: EntityId) {
    const member = await this.dataServices.members.get(memberId);

    const members = await this.dataServices.members.getAllForGroupId(
      member.group.id,
    );

    const hasMembersLeft = members.some((m) => m.isAdmin && m.id !== memberId);
    if (!hasMembersLeft) {
      throw new UnprocessableEntityException();
    }

    return this.dataServices.members.delete(memberId);
  }

  deleteAllMembers() {
    return this.dataServices.members.deleteAll();
  }
}
