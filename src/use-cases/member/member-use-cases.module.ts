import { Module } from '@nestjs/common';
import { DataServicesModule } from 'services/data-services.module';
import { MemberFactoryService } from './member-factory.service';
import { MemberUseCases } from './member-use-cases.service';

@Module({
  imports: [DataServicesModule],
  providers: [MemberFactoryService, MemberUseCases],
  exports: [MemberFactoryService, MemberUseCases],
})
export class MemberUseCasesModule {}
