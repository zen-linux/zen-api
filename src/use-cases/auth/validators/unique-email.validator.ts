import { Injectable, UnprocessableEntityException } from '@nestjs/common';

import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UserUseCases } from 'use-cases/user/user-use-cases.service';

@ValidatorConstraint({ name: 'unique-email', async: true })
@Injectable()
export class UniqueEmailValidator implements ValidatorConstraintInterface {
  constructor(private readonly userUseCases: UserUseCases) {}

  async validate(email: string): Promise<boolean> {
    return this.userUseCases.getUserByEmail(email).then((user) => {
      if (user) {
        throw new UnprocessableEntityException('Email already exists');
      } else {
        return true;
      }
    });
  }
}
