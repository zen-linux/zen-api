import { User } from 'core/entities/user';

type CleanUser = Omit<User, 'password'>;
