import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { RegisterDto } from 'core/dtos/register.dto';
import { User } from 'core/entities/user';
import { UserUseCases } from '../user/user-use-cases.service';
import { CleanUser } from './types/clean-user';

@Injectable()
export class AuthUseCases {
  constructor(
    private userUseCases: UserUseCases,
    private jwtService: JwtService,
  ) {}

  async registerUser(registerDto: RegisterDto): Promise<void> {
    await this.userUseCases.createUser({
      email: registerDto.email,
      password: registerDto.password,
      name: registerDto.username,
    });
  }

  async authenticateUser(email: string, password: string): Promise<CleanUser> {
    let user: User | null = null;
    try {
      user = await this.userUseCases.getUserByEmail(email);
    } catch (error: any) {
      if (!(error instanceof NotFoundException)) {
        throw error;
      }
    }
    if (user && user.password === password) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...userWithoutPassword } = user;
      return userWithoutPassword;
    }
    throw new UnauthorizedException();
  }

  async login(user: CleanUser) {
    const payload: JwtPayload = { sub: user.id };
    return {
      accessToken: this.jwtService.sign(payload),
    };
  }
}
