import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Settings } from 'core/abstracts/settings.abstract';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ApiKeyUseCases } from 'use-cases/api-key/api-key-use-cases.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(settings: Settings, private apiKeyUseCases: ApiKeyUseCases) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: settings.getJwtSecret(),
    });
  }

  async validate(payload: JwtPayload) {
    console.log('jwt validate payload:', payload);
    const user: Express.User = {
      id: payload.sub,
    };
    return user;
  }
}
