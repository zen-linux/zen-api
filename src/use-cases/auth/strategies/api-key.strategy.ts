import { Injectable, NotFoundException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import Strategy from 'passport-headerapikey';
import { ApiKeyUseCases } from 'use-cases/api-key/api-key-use-cases.service';

export const API_KEY_HEADER = 'X-API-KEY';

@Injectable()
export class ApiKeyStrategy extends PassportStrategy(Strategy, 'api-key') {
  constructor(private apiKeyUseCases: ApiKeyUseCases) {
    super({ header: API_KEY_HEADER });
  }

  verify = async (
    key: any,
    verified: (err: Error | null, user?: Object, info?: Object) => void,
  ) => {
    console.log('ApiKeyStrategy verify key:', key);

    try {
      const apiKey = await this.apiKeyUseCases.getApiKeyByKey(key);

      const user: Express.User = {
        id: apiKey.user.id,
      };

      verified(null, user);
    } catch (error: any) {
      if (error instanceof NotFoundException) {
        verified(new NotFoundException());
      }
    }
  };
}
