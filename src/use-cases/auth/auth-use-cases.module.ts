import { OrGuard } from '@nest-lab/or-guard';
import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Settings } from 'core/abstracts/settings.abstract';
import { SettingsModule } from 'services/settings.module';
import { ApiKeyUseCasesModule } from '../api-key/api-key-use-cases.module';
import { UserUseCasesModule } from '../user/user-use-cases.module';
import { AuthUseCases } from './auth-use-cases.service';
import { ApiKeyAuthGuard } from './guards/api-key-auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { ApiKeyStrategy } from './strategies/api-key.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { UniqueEmailValidator } from './validators/unique-email.validator';

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      imports: [SettingsModule],
      inject: [Settings],
      useFactory: (settings: Settings) => ({
        secret: settings.getJwtSecret(),
        signOptions: { expiresIn: '60m' },
      }),
    }),
    UserUseCasesModule,
    ApiKeyUseCasesModule,
    SettingsModule,
  ],
  providers: [
    AuthUseCases,
    JwtStrategy,
    ApiKeyStrategy,
    JwtAuthGuard,
    ApiKeyAuthGuard,
    {
      provide: APP_GUARD,
      useClass: OrGuard([JwtAuthGuard, ApiKeyAuthGuard]),
    },
    UniqueEmailValidator,
  ],
  exports: [AuthUseCases],
})
export class AuthUseCasesModule {}
