import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { CreatePkgDto } from 'core/dtos/pkg.dto';
import { Pkg } from 'core/entities/pkg';
import { RepoService } from '../repo/services/repo.service';
import { PkgFactoryService } from './pkg-factory.service';

@Injectable()
export class PkgUseCases {
  constructor(
    private dataServices: DataServices,
    private pkgFactoryService: PkgFactoryService,
    private repoService: RepoService,
  ) {}

  getAllPkgs(): Promise<Pkg[]> {
    return this.dataServices.pkgs.getAll();
  }

  getAllPkgsByRepoId(repoId: EntityId): Promise<Pkg[]> {
    return this.dataServices.pkgs.getAllByRepoId(repoId);
  }

  getPkgById(id: EntityId): Promise<Pkg | null> {
    return this.dataServices.pkgs.get(id);
  }

  async createPkgForUserId(
    file: Express.Multer.File,
    createPkgDto: CreatePkgDto,
    userId: EntityId,
  ): Promise<Pkg> {
    console.log('file:', file);

    const pkg = await this.pkgFactoryService.create(createPkgDto, file, userId);

    console.log('pkg:', pkg);

    try {
      const createdPkg = await this.dataServices.pkgs.create(pkg);
      await this.repoService.addPackage(pkg.repo, file.path, file.originalname);
      return createdPkg;
    } catch (error: any) {
      console.log('catched error:', error);
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        console.log('haha');
        throw new UnprocessableEntityException();
      }
      throw error;
    }
  }

  async deletePkgForUserId(pkgId: EntityId, userId: EntityId) {
    const pkg = await this.dataServices.pkgs.get(pkgId);

    await this.dataServices.pkgs.deleteForUserId(pkgId, userId);

    await this.repoService.removePackage(pkg);
  }
}
