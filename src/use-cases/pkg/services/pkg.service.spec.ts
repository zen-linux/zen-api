import { Test } from '@nestjs/testing';
import { GzipFileService } from 'use-cases/gzip-file/services/gzip-file.service';
import { ParsedPkgFile } from '../entities/parsed-pkg-file';
import { PkgService } from './pkg.service';

describe('PkgService', () => {
  let pkgService: PkgService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [PkgService, GzipFileService],
    }).compile();

    pkgService = moduleRef.get(PkgService);
  });

  describe('parsePkgFile', () => {
    it('should parse correctly (acl)', async () => {
      const parsedPkgFile = await pkgService.parsePkgFile(
        'misc/rest/acl-2.3.1-18-x86_64.pkg.tar.gz',
      );

      expect(parsedPkgFile).toEqual({
        pkgInfo: {
          name: 'acl',
          desc: 'Access control list utilities, libraries and headers',
          ver: '2.3.1',
          rel: 18,
          size: 256403,
        },
      } as ParsedPkgFile);
    });

    it('should parse correctly (base-files)', async () => {
      const parsedPkgFile = await pkgService.parsePkgFile(
        'misc/testdata/base-files-20230317-2-any.pkg.tar.gz',
      );

      expect(parsedPkgFile).toEqual({
        pkgInfo: {
          name: 'base-files',
          desc: 'Base Linux files',
          ver: '20230317',
          rel: 2,
          size: 3285,
        },
      } as ParsedPkgFile);
    });
  });
});
