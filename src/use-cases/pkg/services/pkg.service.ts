import { Injectable } from '@nestjs/common';
import { GzipFileService } from '../../gzip-file/services/gzip-file.service';
import { ParsedPkgFile } from '../entities/parsed-pkg-file';

@Injectable()
export class PkgService {
  constructor(private readonly gzipFileService: GzipFileService) {}

  async parsePkgFile(filePath: string): Promise<ParsedPkgFile> {
    const parsedPkgFile = new ParsedPkgFile();
    parsedPkgFile.pkgInfo = {
      name: '',
      desc: '',
      ver: '',
      rel: 0,
      size: 0,
    };

    const content = await this.gzipFileService.readContentFromGzipFile(
      filePath,
      (header) => header.name === '.PKGINFO',
    );

    const lines = content[0].content.split('\n');
    for (const line of lines) {
      if (line.startsWith('#')) {
        continue;
      }
      const lineParts = line.split(' = ');
      const key = lineParts[0];

      switch (key) {
        case 'pkgname':
          parsedPkgFile.pkgInfo.name = lineParts[1];
          break;
        case 'pkgdesc':
          parsedPkgFile.pkgInfo.desc = lineParts[1];
          break;
        case 'pkgver':
          const verParts = lineParts[1].split('-');
          parsedPkgFile.pkgInfo.ver = verParts[0];
          parsedPkgFile.pkgInfo.rel = Number.parseInt(verParts[1]);
          break;
        case 'size':
          parsedPkgFile.pkgInfo.size = Number.parseInt(lineParts[1]);
          break;
      }
    }

    return parsedPkgFile;
  }
}
