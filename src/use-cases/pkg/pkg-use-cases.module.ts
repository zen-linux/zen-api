import { Module } from '@nestjs/common';
import { DataServicesModule } from 'services/data-services.module';
import { GzipFileUseCasesModule } from '../gzip-file/gzip-file-use-cases.module';
import { RepoUseCasesModule } from '../repo/repo-use-cases.module';
import { PkgFactoryService } from './pkg-factory.service';
import { PkgUseCases } from './pkg-use-cases.service';
import { PkgService } from './services/pkg.service';

@Module({
  imports: [DataServicesModule, RepoUseCasesModule, GzipFileUseCasesModule],
  providers: [PkgFactoryService, PkgUseCases, PkgService],
  exports: [PkgFactoryService, PkgUseCases],
})
export class PkgUseCasesModule {}
