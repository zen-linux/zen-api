import { Injectable } from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { CreatePkgDto } from 'core/dtos/pkg.dto';
import { Pkg } from 'core/entities/pkg';
import { ParsedPkgFile } from './entities/parsed-pkg-file';
import { PkgService } from './services/pkg.service';

@Injectable()
export class PkgFactoryService {
  constructor(
    private dataServices: DataServices,
    private pkgService: PkgService,
  ) {}

  async create(
    createDto: CreatePkgDto,
    file: Express.Multer.File,
    userId: EntityId,
  ): Promise<Pkg> {
    const repo = await this.dataServices.repos.getByIdForUserId(
      createDto.repoId,
      userId,
    );

    const parsedPkgFile: ParsedPkgFile = await this.pkgService.parsePkgFile(
      file.path,
    );

    const pkg = new Pkg({
      name: parsedPkgFile.pkgInfo.name,
      desc: parsedPkgFile.pkgInfo.desc,
      ver: parsedPkgFile.pkgInfo.ver,
      rel: parsedPkgFile.pkgInfo.rel,
      size: parsedPkgFile.pkgInfo.size,
      repo,
    });
    return pkg;
  }
}
