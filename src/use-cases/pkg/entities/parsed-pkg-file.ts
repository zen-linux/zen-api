export class ParsedPkgFile {
  pkgInfo!: {
    name: string;
    desc: string;
    ver: string;
    rel: number;
    size: number;
  };
}
