import { Injectable } from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from 'core/dtos/user.dto';
import { User } from 'core/entities/user';
import { FactoryService } from 'core/services/factory-service';

@Injectable()
export class UserFactoryService extends FactoryService<User> {
  async create(createDto: CreateUserDto): Promise<User> {
    const user = new User({
      name: createDto.name,
      email: createDto.email,
      password: createDto.password,
    });
    return user;
  }

  async update(id: EntityId, updateDto: UpdateUserDto): Promise<User> {
    const user = await this.dataServices.users.get(id);

    if (updateDto.name !== undefined) {
      user.name = updateDto.name;
    }
    if (updateDto.email !== undefined) {
      user.email = updateDto.email;
    }
    if (updateDto.password !== undefined) {
      user.password = updateDto.password;
    }

    return user;
  }
}
