import { Module } from '@nestjs/common';
import { DataServicesModule } from 'services/data-services.module';
import { UserFactoryService } from './user-factory.service';
import { UserUseCases } from './user-use-cases.service';

@Module({
  imports: [DataServicesModule],
  providers: [UserFactoryService, UserUseCases],
  exports: [UserFactoryService, UserUseCases],
})
export class UserUseCasesModule {}
