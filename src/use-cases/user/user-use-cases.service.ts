import { Injectable, NotFoundException } from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { CreateUserDto, UpdateUserDto } from 'core/dtos/user.dto';
import { Group } from 'core/entities/group';
import { Member } from 'core/entities/member';
import { User } from 'core/entities/user';
import { UserFactoryService } from './user-factory.service';

@Injectable()
export class UserUseCases {
  constructor(
    private dataServices: DataServices,
    private userFactoryService: UserFactoryService,
  ) {}

  getAllUsers(): Promise<User[]> {
    return this.dataServices.users.getAll();
  }

  getUserById(id: EntityId): Promise<User | null> {
    return this.dataServices.users.get(id);
  }

  async getUserByEmail(email: string): Promise<User | null> {
    try {
      const user = await this.dataServices.users.getByEmail(email);
      return user;
    } catch (error: any) {
      if (error instanceof NotFoundException) {
        return null;
      }
      throw error;
    }
  }

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    const group = await this.dataServices.groups.create(
      new Group({
        name: createUserDto.name,
        description: `Primary group of ${createUserDto.name}`,
      }),
    );

    const user = await this.dataServices.users.create(
      await this.userFactoryService.create(createUserDto),
    );

    await this.dataServices.members.create(
      new Member({ user, group, isAdmin: true }),
    );

    return user;
  }

  async updateUser(id: EntityId, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.userFactoryService.update(id, updateUserDto);
    return this.dataServices.users.update(id, user);
  }

  deleteAllUsers(): Promise<void> {
    return this.dataServices.users.deleteAll();
  }
}
