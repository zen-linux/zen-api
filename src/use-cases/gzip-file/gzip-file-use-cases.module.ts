import { Module } from '@nestjs/common';
import { GzipFileService } from './services/gzip-file.service';

@Module({
  providers: [GzipFileService],
  exports: [GzipFileService],
})
export class GzipFileUseCasesModule {}
