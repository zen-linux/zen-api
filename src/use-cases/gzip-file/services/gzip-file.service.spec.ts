import { Test } from '@nestjs/testing';
import { readFile } from 'fs/promises';
import { GzipFileService } from './gzip-file.service';

describe('GzipFileService', () => {
  let gzipFileService: GzipFileService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [GzipFileService],
    }).compile();

    gzipFileService = moduleRef.get(GzipFileService);
  });

  describe('readContentFromPkgFile', () => {
    it('should return file content from a pkg file (acl)', async () => {
      const referenceContent = await readFile(
        'misc/testdata/acl-pkginfo',
        'utf-8',
      );
      const referenceResult = [{ path: '.PKGINFO', content: referenceContent }];

      const actualContent = await gzipFileService.readContentFromGzipFile(
        'misc/rest/acl-2.3.1-18-x86_64.pkg.tar.gz',
        (header) => header.name === '.PKGINFO',
      );

      expect(actualContent).toEqual(referenceResult);
    });

    it('should return file content from a pkg file (base-files)', async () => {
      const referenceContent = await readFile(
        'misc/testdata/base-files-pkginfo',
        'utf-8',
      );
      const referenceResult = [{ path: '.PKGINFO', content: referenceContent }];

      const actualContent = await gzipFileService.readContentFromGzipFile(
        'misc/testdata/base-files-20230317-2-any.pkg.tar.gz',
        (header) => header.name === '.PKGINFO',
      );

      expect(actualContent).toEqual(referenceResult);
    });

    it('should throw errror when path is invalid', async () => {
      expect.assertions(1);

      try {
        await gzipFileService.readContentFromGzipFile(
          'invalidPath',
          (header) => header.name === '.PKGINFO',
        );
      } catch (error) {
        expect(error).toBeTruthy();
      }
    });

    it('should throw errror when file is not gzipped', async () => {
      expect.assertions(1);

      try {
        await gzipFileService.readContentFromGzipFile(
          'misc/testdata/acl-pkginfo',
          (header) => header.name === '.PKGINFO',
        );
      } catch (error) {
        expect(error).toBeTruthy();
      }
    });
  });
});
