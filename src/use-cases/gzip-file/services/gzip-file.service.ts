import { Injectable } from '@nestjs/common';
import * as fsp from 'fs/promises';
import { StringDecoder } from 'string_decoder';
import * as tarStream from 'tar-stream';
import * as zlib from 'zlib';

@Injectable()
export class GzipFileService {
  async readContentFromGzipFile(
    gzipFilePath: string,
    filter: (header: tarStream.Headers) => boolean,
  ): Promise<{ path: string; content: string }[]> {
    return new Promise(async (resolve, reject) => {
      const content: { path: string; content: string }[] = [];

      try {
        const fileHandle = await fsp.open(gzipFilePath);
        const readStream = fileHandle.createReadStream();

        readStream.on('error', (error) => {
          reject(new Error(`stream error: ${error.message}`));
        });

        const gunzip = zlib.createGunzip();

        gunzip.on('error', (error) => {
          reject(new Error(`gzip error: ${error.message}`));
        });

        const tarExtract = tarStream.extract();

        tarExtract.on('entry', (header, stream, next) => {
          stream.resume();

          if (filter(header)) {
            const decoder = new StringDecoder();

            stream.on('data', (chunk: Buffer) => {
              const text = decoder.write(chunk);
              content.push({ path: header.name, content: text });
              //content += text;

              stream.resume();
            });

            stream.on('end', () => {
              next();
            });
          } else {
            next();
          }
        });

        tarExtract.on('finish', () => {
          resolve(content);
        });

        readStream.pipe(gunzip).pipe(tarExtract);
      } catch (error: any) {
        reject(error);
      }
    });
  }
}
