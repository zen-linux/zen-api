import { Injectable } from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { CreateApiKeyDto, UpdateApiKeyDto } from 'core/dtos/api-key.dto';
import { ApiKey } from 'core/entities/api-key';
import { ApiKeyFactoryService } from './api-key-factory.service';

@Injectable()
export class ApiKeyUseCases {
  constructor(
    private dataServices: DataServices,
    private apiKeyFactoryService: ApiKeyFactoryService,
  ) {}

  getAllApiKeysForUserId(userId: EntityId): Promise<ApiKey[]> {
    return this.dataServices.apikeys.getAllByUserId(userId);
  }

  getApiKeyById(id: EntityId): Promise<ApiKey> {
    return this.dataServices.apikeys.get(id);
  }

  getApiKeyByKey(key: string): Promise<ApiKey> {
    return this.dataServices.apikeys.getByKey(key);
  }

  async createApiKeyForUserId(
    userId: EntityId,
    createApiKeyDto: CreateApiKeyDto,
  ): Promise<ApiKey> {
    const apiKey = await this.dataServices.apikeys.create(
      await this.apiKeyFactoryService.create(createApiKeyDto, userId),
    );
    return apiKey;
  }

  async updateApiKeyForUserId(
    id: EntityId,
    updateApiKeyDto: UpdateApiKeyDto,
    userId: EntityId,
  ): Promise<ApiKey> {
    const apiKey = await this.apiKeyFactoryService.update(id, updateApiKeyDto);
    return this.dataServices.apikeys.update(id, apiKey);
  }

  deleteByIdForUserId(id: EntityId, userId: EntityId): Promise<void> {
    return this.dataServices.apikeys.deleteByIdForUserId(id, userId);
  }

  deleteAllForUserId(userId: EntityId): Promise<void> {
    return this.dataServices.apikeys.deleteAllForUserId(userId);
  }
}
