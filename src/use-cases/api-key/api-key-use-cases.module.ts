import { Module } from '@nestjs/common';
import { DataServicesModule } from 'services/data-services.module';
import { ApiKeyFactoryService } from './api-key-factory.service';
import { ApiKeyUseCases } from './api-key-use-cases.service';

@Module({
  imports: [DataServicesModule],
  providers: [ApiKeyFactoryService, ApiKeyUseCases],
  exports: [ApiKeyFactoryService, ApiKeyUseCases],
})
export class ApiKeyUseCasesModule {}
