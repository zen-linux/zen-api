import { Injectable } from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { CreateApiKeyDto, UpdateApiKeyDto } from 'core/dtos/api-key.dto';
import { ApiKey } from 'core/entities/api-key';
import { generateApiKey } from 'generate-api-key';

@Injectable()
export class ApiKeyFactoryService {
  constructor(private dataServices: DataServices) {}

  async create(createDto: CreateApiKeyDto, userId: EntityId): Promise<ApiKey> {
    const user = await this.dataServices.users.get(userId);

    const apiKey = new ApiKey({
      key: generateApiKey() as string,
      description: createDto.description,
      user,
    });

    return apiKey;
  }

  async update(id: EntityId, updateDto: UpdateApiKeyDto): Promise<ApiKey> {
    const apiKey = await this.dataServices.apikeys.get(id);

    if (updateDto.description !== undefined) {
      apiKey.description = updateDto.description;
    }

    return apiKey;
  }
}
