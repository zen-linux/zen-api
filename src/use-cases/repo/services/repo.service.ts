import { Injectable, Logger } from '@nestjs/common';
import { exec } from 'child_process';
import { Settings } from 'core/abstracts/settings.abstract';
import { Pkg } from 'core/entities/pkg';
import { Repo } from 'core/entities/repo';
import * as fs from 'fs';
import * as fsp from 'fs/promises';
import * as path from 'path';
import { GzipFileService } from '../../gzip-file/services/gzip-file.service';
import { PkgDesc } from '../entities/pkg-desc';

export const protectedRepoNames = ['core', 'pkgs'];

@Injectable()
export class RepoService {
  private readonly logger = new Logger(RepoService.name);

  constructor(
    private readonly settings: Settings,
    private readonly gzipFileService: GzipFileService,
  ) {}

  getReposPath(): string {
    return path.resolve(this.settings.getDataPath(), 'repos');
  }

  getRepoPath(groupName: string, repoName: string): string {
    return path.join(this.getReposPath(), groupName, repoName);
  }

  getDbPath(repoPath: string, repoName: string): string {
    return path.join(repoPath, `${repoName}.db.tar.gz`);
  }

  getFilesPath(repoPath: string, repoName: string): string {
    return path.join(repoPath, `${repoName}.files.tar.gz`);
  }

  getRepoFilePath(
    groupName: string,
    repoName: string,
    fileName: string,
  ): string {
    return path.join(this.getRepoPath(groupName, repoName), fileName);
  }

  async createRepoDir(repoPath: string) {
    await fsp.mkdir(repoPath, { recursive: true });
  }

  async removeRepoDir(repoPath: string) {
    await fsp.rm(repoPath, { recursive: true });

    const parentPath = path.resolve(repoPath, '..');
    const files = await fsp.readdir(parentPath);
    console.log('files:', files);
    if (files.length === 0) {
      console.log('removing:', parentPath);
      await fsp.rm(parentPath, { recursive: true });
    }
  }

  async addPackage(
    repo: Repo,
    filePath: string,
    fileName: string,
  ): Promise<void> {
    const repoPath = this.getRepoPath(repo.group.name, repo.name);
    const dbPath = this.getDbPath(repoPath, repo.name);

    const destPkgPath = path.join(repoPath, fileName);

    await fsp.copyFile(filePath, destPkgPath);
    await fsp.rm(filePath);
    await this.exec(`repo-add '${dbPath}' '${destPkgPath}'`);

    this.logger.log(`added package '${fileName}' to repo '${repo.name}'`);
  }

  async createEmptyRepoDb(repo: Repo) {
    const repoPath = this.getRepoPath(repo.group.name, repo.name);
    await this.createRepoDir(repoPath);

    const dbPath = this.getDbPath(repoPath, repo.name);

    const output = await this.exec(`repo-add '${dbPath}'`);
    console.log('output:', output);

    await this.removeOldFiles(repo);
  }

  async removeOldFiles(repo: Repo): Promise<void> {
    const repoPath = this.getRepoPath(repo.group.name, repo.name);
    const dbPath = this.getDbPath(repoPath, repo.name);
    const filesPath = this.getFilesPath(repoPath, repo.name);

    await fsp.rm(`${dbPath}.old`, { force: true });
    await fsp.rm(`${filesPath}.old`, { force: true });
  }

  async removePackage(pkg: Pkg): Promise<void> {
    const repoPath = this.getRepoPath(pkg.repo.group.name, pkg.repo.name);
    const dbPath = this.getDbPath(repoPath, pkg.repo.name);

    const pkgName = pkg.name.replace('.pkg.tar.gz', '');

    await this.exec(`repo-remove '${dbPath}' ${pkgName}`);

    this.logger.log(
      `removed package '${pkgName}' from repo '${pkg.repo.group.name}/${pkg.repo.name}'`,
    );
  }

  cleanRepo(repoPath: string, repoDbPath: string) {
    return Promise.all([
      this.readPackagesFromRepoFile(repoDbPath),
      fsp.readdir(repoPath),
    ]).then((results) => {
      const repoPackages = results[0];
      const packageFiles = results[1];

      console.log('repoPackages:', repoPackages);
      console.log('packageFiles:', packageFiles);

      const savedFiles: string[] = [];
      const deletedFiles: string[] = [];

      for (const pkgFile of packageFiles) {
        if (!pkgFile.endsWith('.pkg.tar.gz')) {
          // ignore other files
          continue;
        }

        if (repoPackages.includes(pkgFile)) {
          savedFiles.push(pkgFile);
          continue;
        }

        fs.unlinkSync(path.join(repoPath, pkgFile));

        deletedFiles.push(pkgFile);
      }

      return { savedFiles, deletedFiles };
    });
  }

  async readPackagesFromRepoFile(repoDbPath: string): Promise<string[]> {
    const packages = await this.gzipFileService.readContentFromGzipFile(
      repoDbPath,
      (header) => header.name.endsWith('/desc'),
    );

    return packages.map((entry) => this.parsePkgDesc(entry.content).fileName);
  }

  parsePkgDesc(desc: string): PkgDesc {
    const pkgDesc = new PkgDesc();

    const regex = /%([^\n]+)%\n([^\n]+|(?:[^\n]+\n[^\n]+)*)\n\n/gms;

    let m: RegExpExecArray | null;
    while ((m = regex.exec(desc)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }

      // The result can be accessed through the `m`-variable.

      const key = m[1];
      //console.log('key:', key);

      const value = m[2];
      //console.log('value:', value);

      switch (key) {
        case 'FILENAME':
          pkgDesc.fileName = value;
          break;
        case 'NAME':
          pkgDesc.name = value;
          break;
        case 'BASE':
          pkgDesc.base = value;
          break;
        case 'VERSION':
          const verParts = value.split('-');
          pkgDesc.ver = verParts[0];
          pkgDesc.rel = Number.parseInt(verParts[1]);
          break;
        case 'ARCH':
          pkgDesc.arch = value;
          break;
        case 'DESC':
          pkgDesc.desc = value;
          break;
        case 'URL':
          pkgDesc.url = value;
          break;
        case 'MD5SUM':
          pkgDesc.md5 = value;
          break;
        case 'SHA256SUM':
          pkgDesc.sha256 = value;
          break;
        case 'CSIZE':
          pkgDesc.cSize = Number.parseInt(value);
          break;
        case 'ISIZE':
          pkgDesc.iSize = Number.parseInt(value);
          break;
        case 'LICENSE':
          pkgDesc.license = value;
          break;
        case 'BUILDDATE':
          pkgDesc.buildDate = new Date(Number.parseInt(value) * 1000);
          break;
        case 'PACKAGER':
          pkgDesc.packager = value;
          break;
        case 'DEPENDS':
          pkgDesc.depends = value.split('\n');
          break;
        case 'MAKEDEPENDS':
          pkgDesc.makeDepends = value.split('\n');
          break;
      }
    }

    return pkgDesc;
  }

  private exec(command: string): Promise<{ stdout: string; stderr: string }> {
    return new Promise((resolve, reject) => {
      exec(command, (error, stdout, stderr) => {
        if (error) {
          reject({ error, stdout, stderr });
        }
        resolve({ stdout, stderr });
      });
    });
  }
}
