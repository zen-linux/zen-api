import { Test } from '@nestjs/testing';
import { Settings } from 'core/abstracts/settings.abstract';
import { copyFile, mkdtemp, rm } from 'fs/promises';
import { tmpdir } from 'os';
import { join } from 'path';
import { GzipFileService } from 'use-cases/gzip-file/services/gzip-file.service';
import { RepoService } from './repo.service';

describe('RepoService', () => {
  let repoService: RepoService;

  let tempRepoPath: string;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        RepoService,
        GzipFileService,
        {
          provide: Settings,
          useValue: {
            getDataPath: () => 'super',
          },
        },
      ],
    }).compile();

    repoService = moduleRef.get(RepoService);

    tempRepoPath = await mkdtemp(join(tmpdir(), 'zen-api-repo'));
  });

  afterEach(async () => {
    rm(tempRepoPath, { recursive: true });
  });

  describe('cleanRepo', () => {
    it('should remove files not anymore in db', async () => {
      const tempRepoDbPath = `${tempRepoPath}/test.db.tar.gz`;

      await copyFile('misc/testdata/test.db.tar.gz', tempRepoDbPath);
      await copyFile(
        'misc/testdata/base-files-20230317-2-any.pkg.tar.gz',
        `${tempRepoPath}/base-files-20230317-2-any.pkg.tar.gz`,
      );
      await copyFile(
        'misc/testdata/limine-4.20230314.0-6-x86_64.pkg.tar.gz',
        `${tempRepoPath}/limine-4.20230314.0-6-x86_64.pkg.tar.gz`,
      );

      const result = await repoService.cleanRepo(tempRepoPath, tempRepoDbPath);

      expect(result).toEqual({
        deletedFiles: ['base-files-20230317-2-any.pkg.tar.gz'],
        savedFiles: ['limine-4.20230314.0-6-x86_64.pkg.tar.gz'],
      });
    });
  });

  describe('readPackagesFromRepoFile', () => {
    it('should return correct package list (only limine)', async () => {
      const packages = await repoService.readPackagesFromRepoFile(
        'misc/testdata/test.db.tar.gz',
      );

      expect(packages).toEqual(['limine-4.20230314.0-6-x86_64.pkg.tar.gz']);
    });
  });
});
