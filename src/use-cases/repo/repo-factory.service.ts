import { Injectable } from '@nestjs/common';
import { CreateRepoDto, UpdateRepoDto } from 'core/dtos/repo.dto';
import { Repo } from 'core/entities/repo';
import { FactoryService } from 'core/services/factory-service';

@Injectable()
export class RepoFactoryService extends FactoryService<Repo> {
  async create(createDto: CreateRepoDto): Promise<Repo> {
    const group = await this.dataServices.groups.get(createDto.groupId);

    const repo = new Repo({
      name: createDto.name,
      group,
    });
    return repo;
  }

  async update(id: EntityId, updateDto: UpdateRepoDto): Promise<Repo> {
    const repo = await this.dataServices.repos.get(id);

    if (updateDto.name !== undefined) {
      repo.name = updateDto.name;
    }

    return repo;
  }
}
