import { Module } from '@nestjs/common';
import { DataServicesModule } from 'services/data-services.module';
import { SettingsModule } from 'services/settings.module';
import { GzipFileUseCasesModule } from 'use-cases/gzip-file/gzip-file-use-cases.module';
import { RepoFactoryService } from './repo-factory.service';
import { RepoUseCases } from './repo-use-cases.service';
import { RepoService } from './services/repo.service';

@Module({
  imports: [DataServicesModule, SettingsModule, GzipFileUseCasesModule],
  providers: [RepoFactoryService, RepoUseCases, RepoService],
  exports: [RepoFactoryService, RepoUseCases, RepoService],
})
export class RepoUseCasesModule {}
