import {
  Injectable,
  NotFoundException,
  StreamableFile,
  UnprocessableEntityException,
} from '@nestjs/common';
import { DataServices } from 'core/abstracts/data-services.abstract';
import { CreateRepoDto, UpdateRepoDto } from 'core/dtos/repo.dto';
import { Repo } from 'core/entities/repo';
import { filesize } from 'filesize';
import { createReadStream } from 'fs';
import * as fs from 'fs/promises';
import { join } from 'path';
import { RepoFactoryService } from './repo-factory.service';
import { protectedRepoNames, RepoService } from './services/repo.service';

@Injectable()
export class RepoUseCases {
  constructor(
    private dataServices: DataServices,
    private repoFactoryService: RepoFactoryService,
    private repoService: RepoService,
  ) {}

  getAllRepos(): Promise<Repo[]> {
    return this.dataServices.repos.getAll();
  }

  getAllReposForUserId(userId: EntityId): Promise<Repo[]> {
    return this.dataServices.repos.getAllForUserId(userId);
  }

  getAllReposByGroupId(groupId: EntityId): Promise<Repo[]> {
    return this.dataServices.repos.getAllByGroupId(groupId);
  }

  getRepoById(id: EntityId): Promise<Repo> {
    return this.dataServices.repos.get(id);
  }

  async cleanRepoForUserId(
    repoId: EntityId,
    userId: EntityId,
  ): Promise<{ savedFiles: string[]; deletedFiles: string[] }> {
    const repo = await this.dataServices.repos.getByIdForUserId(repoId, userId);

    const repoPath = this.repoService.getRepoPath(repo.group.name, repo.name);
    const repoDbPath = this.repoService.getDbPath(repoPath, repo.name);

    return this.repoService.cleanRepo(repoPath, repoDbPath);
  }

  async createRepoForUserId(
    userId: EntityId,
    createRepoDto: CreateRepoDto,
  ): Promise<Repo> {
    const group = await this.dataServices.groups.getByIdForUserId(
      createRepoDto.groupId,
      userId,
    );

    const existingRepos = await this.dataServices.repos.getAllByGroupId(
      group.id,
    );
    if (existingRepos.some((repo) => repo.name === createRepoDto.name)) {
      throw new UnprocessableEntityException(
        'repo with this name already exists',
      );
    }

    const repo = await this.repoFactoryService.create(createRepoDto);

    await this.repoService.createEmptyRepoDb(repo);

    return this.dataServices.repos.create(repo);
  }

  async updateRepo(id: EntityId, updateRepoDto: UpdateRepoDto): Promise<Repo> {
    const repo = await this.repoFactoryService.update(id, updateRepoDto);
    return this.dataServices.repos.update(id, repo);
  }

  async deleteRepoForUserId(repoId: EntityId, userId: EntityId): Promise<void> {
    const repo = await this.dataServices.repos.getByIdForUserId(repoId, userId);

    if (repo.group.name === 'zen' && protectedRepoNames.includes(repo.name)) {
      throw new UnprocessableEntityException(
        'cannot delete a protected repository',
      );
    }

    // delete dirs
    const repoPath = this.repoService.getRepoPath(repo.group.name, repo.name);
    await this.repoService.removeRepoDir(repoPath);

    return this.dataServices.repos.delete(repoId);
  }

  async serveRepoFile(
    groupName: string,
    repoName: string,
    fileName: string,
    modifiedSinceHeader: string | undefined,
  ): Promise<StreamableFile | null> {
    const filePath = this.repoService.getRepoFilePath(
      groupName,
      repoName,
      fileName,
    );

    /*console.log(
      `serving repo file '${groupName}/${repoName}/${fileName}', filePath: '${filePath}'`,
    );*/

    try {
      const stat = await fs.stat(filePath);

      if (modifiedSinceHeader !== undefined) {
        const modifiedSinceDate = new Date(modifiedSinceHeader);
        if (modifiedSinceDate >= stat.mtime) {
          return null;
        }
      }
    } catch (error: any) {
      throw new NotFoundException();
    }

    return new StreamableFile(createReadStream(filePath));
  }

  async serveRepoIndex(groupName: string, repoName: string): Promise<any> {
    const repoPath = this.repoService.getRepoPath(groupName, repoName);

    try {
      await fs.stat(repoPath);
    } catch (error: any) {
      throw new NotFoundException();
    }

    const fileNames = await fs.readdir(repoPath);

    const files = await Promise.all(
      fileNames.map(async (fileName) => {
        const fullPath = join(repoPath, fileName);
        const fileStat = await fs.stat(fullPath);

        return {
          name: fileName,
          size: filesize(fileStat.size),
          mtime: fileStat.mtime.toLocaleString(),
        };
      }),
    );

    return {
      baseHref: `/${groupName}/${repoName}/`,
      repoName: `${groupName}/${repoName}`,
      files,
    };
  }
}
