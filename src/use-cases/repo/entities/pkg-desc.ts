export class PkgDesc {
  fileName!: string;
  name!: string;
  base!: string;
  ver!: string;
  rel!: number;
  desc!: string;
  arch!: string;
  cSize!: number;
  iSize!: number;
  url!: string;
  md5!: string;
  sha256!: string;
  license!: string;
  buildDate!: Date;
  packager!: string;
  depends!: string[];
  makeDepends!: string[];
}
