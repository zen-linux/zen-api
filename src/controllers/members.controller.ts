import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiTags,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import {
  CreateMemberDto,
  MemberResponseDto,
  MembersResponseDto,
  UpdateMemberDto,
} from 'core/dtos/member.dto';
import { MemberUseCases } from 'use-cases/member/member-use-cases.service';

@ApiBearerAuth()
@ApiTags('Members')
@Controller()
export class MembersController {
  constructor(private memberUseCases: MemberUseCases) {}

  @Get('groups/:groupId/members')
  async getAllForGroupId(
    @Param('groupId') groupId: EntityId,
  ): Promise<MembersResponseDto> {
    const members = await this.memberUseCases.getAllMembersForGroupId(groupId);
    return {
      members: members.map((member) => ({
        id: member.id,
        isAdmin: member.isAdmin,
        group: {
          id: member.group.id,
          name: member.group.name,
          description: member.group.description,
        },
        user: {
          id: member.user.id,
          name: member.user.name,
          email: member.user.email,
        },
      })),
    };
  }

  @Get('members/:memberId')
  async getById(
    @Param('memberId') memberId: EntityId,
  ): Promise<MemberResponseDto> {
    const member = await this.memberUseCases.getMemberById(memberId);
    if (member) {
      return {
        id: member.id,
        isAdmin: member.isAdmin,
        group: {
          id: member.group.id,
          name: member.group.name,
          description: member.group.description,
        },
        user: {
          id: member.user.id,
          email: member.user.email,
          name: member.user.name,
        },
      };
    }
    throw new NotFoundException();
  }

  @Post('groups/:groupId/members')
  async createMember(
    @Param('groupId') groupId: EntityId,
    @Body() createMemberDto: CreateMemberDto,
  ): Promise<MemberResponseDto> {
    const member = await this.memberUseCases.createMember(
      groupId,
      createMemberDto,
    );
    return {
      id: member.id,
      isAdmin: member.isAdmin,
      group: {
        id: member.group.id,
        name: member.group.name,
        description: member.group.description,
      },
      user: {
        id: member.user.id,
        email: member.user.email,
        name: member.user.name,
      },
    };
  }

  @Put('members/:memberId')
  async updateMember(
    @Param('memberId') memberId: EntityId,
    @Body() updateMemberDto: UpdateMemberDto,
  ): Promise<MemberResponseDto> {
    const member = await this.memberUseCases.updateMember(
      memberId,
      updateMemberDto,
    );
    return {
      id: member.id,
      isAdmin: member.isAdmin,
      group: {
        id: member.group.id,
        name: member.group.name,
        description: member.group.description,
      },
      user: {
        id: member.user.id,
        email: member.user.email,
        name: member.user.name,
      },
    };
  }

  @ApiUnprocessableEntityResponse()
  @Delete('members/:memberId')
  deleteMember(@Param('memberId') memberId: EntityId) {
    return this.memberUseCases.deleteMember(memberId);
  }
}
