import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthUserId } from 'core/decorators/auth-user-id.decorator';
import {
  CreateGroupDto,
  GroupResponseDto,
  GroupsResponseDto,
  UpdateGroupDto,
} from 'core/dtos/group.dto';
import { Public } from 'use-cases/auth/decorators/public.decorator';
import { GroupUseCases } from 'use-cases/group/group-use-cases.service';

@ApiTags('Groups')
@Controller('groups')
export class GroupsController {
  constructor(private groupUseCases: GroupUseCases) {}

  @Public()
  @Get()
  async getAll(): Promise<GroupsResponseDto> {
    const groups = await this.groupUseCases.getAllGroups();
    return {
      groups: groups.map((group) => ({
        id: group.id,
        name: group.name,
        description: group.description,
      })),
    };
  }

  @Public()
  @Get(':groupId')
  async getById(
    @Param('groupId') groupId: EntityId,
  ): Promise<GroupResponseDto> {
    const group = await this.groupUseCases.getGroupById(groupId);
    if (group) {
      return {
        id: group.id,
        name: group.name,
        description: group.description,
      };
    }
    throw new NotFoundException();
  }

  @ApiBearerAuth()
  @Post()
  async createGroup(
    @AuthUserId() userId: EntityId,
    @Body() createGroupDto: CreateGroupDto,
  ): Promise<GroupResponseDto> {
    const group = await this.groupUseCases.createGroup(userId, createGroupDto);
    return {
      id: group.id,
      name: group.name,
      description: group.description,
    };
  }

  @ApiBearerAuth()
  @Put(':groupId')
  async updateGroup(
    @Param('groupId') groupId: EntityId,
    @Body() updateGroupDto: UpdateGroupDto,
  ): Promise<GroupResponseDto> {
    const group = await this.groupUseCases.updateGroup(groupId, updateGroupDto);
    return {
      id: group.id,
      name: group.name,
      description: group.description,
    };
  }

  @ApiBearerAuth()
  @Delete(':groupId')
  deleteGroup(@Param('groupId') groupId: EntityId) {
    return this.groupUseCases.deleteGroup(groupId);
  }
}
