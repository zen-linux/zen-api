import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Put,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthUserId } from 'core/decorators/auth-user-id.decorator';
import { UpdateUserDto, UserResponseDto } from 'core/dtos/user.dto';
import { UserUseCases } from 'use-cases/user/user-use-cases.service';

@ApiBearerAuth()
@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private userUseCases: UserUseCases) {}

  /*@Get()
  async getAll() {
    return this.userUseCases.getAllUsers();
  }*/

  @Get('self')
  async authenticatedUser(
    @AuthUserId() userId: EntityId,
  ): Promise<UserResponseDto> {
    const user = await this.userUseCases.getUserById(userId);
    if (user) {
      return {
        id: user.id,
        email: user.email,
        name: user.name,
      };
    }
    throw new NotFoundException();
  }

  /*
  @Get(':userId')
  async getById(@Param('userId') userId: EntityId): Promise<UserResponseDto> {
    const user = await this.userUseCases.getUserById(userId);
    if (user) {
      return {
        email: user.email,
        name: user.name,
      };
    }
    throw new NotFoundException();
  }*/

  /*@Post()
  createUser(@Body() createUserDto: CreateUserDto) {
    return this.userUseCases.createUser(createUserDto);
  }*/

  @Put(':userId')
  async updateUser(
    @Param('userId') userId: EntityId,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<UserResponseDto> {
    const user = await this.userUseCases.updateUser(userId, updateUserDto);
    return {
      id: user.id,
      email: user.email,
      name: user.name,
    };
  }

  /*@Delete()
  deleteAllUsers() {
    return this.userUseCases.deleteAllUsers();
  }*/
}
