import {
  Controller,
  Get,
  HttpStatus,
  Param,
  Render,
  Req,
  Res,
  StreamableFile,
  VERSION_NEUTRAL,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { Public } from 'use-cases/auth/decorators/public.decorator';
import { RepoUseCases } from 'use-cases/repo/repo-use-cases.service';

@ApiTags('Pacman')
@Controller({ version: VERSION_NEUTRAL })
export class PacmanController {
  constructor(private repoUseCases: RepoUseCases) {}

  @Get(':groupName/:repoName/:fileName')
  @Public()
  async serveRepo(
    @Param('groupName') groupName: string,
    @Param('repoName') repoName: string,
    @Param('fileName') fileName: string,
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ): Promise<StreamableFile | undefined> {
    const value = await this.repoUseCases.serveRepoFile(
      groupName,
      repoName,
      fileName,
      req.headers['if-modified-since'],
    );

    if (value === null) {
      res.status(HttpStatus.NOT_MODIFIED);
      return;
    }

    return value;
  }

  @Get(':groupName/:repoName')
  @Public()
  @Render('repo-index')
  repoIndex(
    @Param('groupName') groupName: string,
    @Param('repoName') repoName: string,
  ) {
    return this.repoUseCases.serveRepoIndex(groupName, repoName);
  }
}
