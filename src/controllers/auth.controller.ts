import { Body, Controller, Post } from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { LoginDto, LoginResponseDto } from 'core/dtos/login.dto';
import { RegisterDto } from 'core/dtos/register.dto';
import { AuthUseCases } from 'use-cases/auth/auth-use-cases.service';
import { Public } from 'use-cases/auth/decorators/public.decorator';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private authUseCases: AuthUseCases) {}

  @Public()
  @ApiCreatedResponse({
    description: 'Successfully authenticated',
    type: LoginResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Invalid credentials' })
  @Post('register')
  register(@Body() registerDto: RegisterDto): Promise<void> {
    return this.authUseCases.registerUser(registerDto);
  }

  @Public()
  @ApiCreatedResponse({
    description: 'Successfully authenticated',
    type: LoginResponseDto,
  })
  @ApiUnauthorizedResponse({ description: 'Invalid credentials' })
  @Post('login')
  async login(@Body() loginDto: LoginDto): Promise<LoginResponseDto> {
    const user = await this.authUseCases.authenticateUser(
      loginDto.email,
      loginDto.password,
    );
    return this.authUseCases.login(user);
  }
}
