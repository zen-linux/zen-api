import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthUserId } from 'core/decorators/auth-user-id.decorator';
import {
  CleanRepoResponseDto,
  CreateRepoDto,
  RepoResponseDto,
  ReposResponseDto,
  UpdateRepoDto,
} from 'core/dtos/repo.dto';
import { Public } from 'use-cases/auth/decorators/public.decorator';
import { RepoUseCases } from 'use-cases/repo/repo-use-cases.service';

@ApiTags('Repositories')
@Controller()
export class ReposController {
  constructor(private repoUseCases: RepoUseCases) {}

  @Public()
  @Get('repos')
  async getAll(): Promise<ReposResponseDto> {
    const repos = await this.repoUseCases.getAllRepos();
    return {
      repos: repos.map((repo) => ({
        id: repo.id,
        name: repo.name,
        group: {
          id: repo.group.id,
          name: repo.group.name,
          description: repo.group.description,
        },
      })),
    };
  }

  @Public()
  @Get('groups/:groupId/repos')
  async getAllForGroup(
    @Param('groupId') groupId: EntityId,
  ): Promise<ReposResponseDto> {
    const repos = await this.repoUseCases.getAllReposByGroupId(groupId);
    return {
      repos: repos.map((repo) => ({
        id: repo.id,
        name: repo.name,
        group: {
          id: repo.group.id,
          name: repo.group.name,
          description: repo.group.description,
        },
      })),
    };
  }

  @Public()
  @Get('repos/:repoId')
  async getById(@Param('repoId') repoId: EntityId): Promise<RepoResponseDto> {
    const repo = await this.repoUseCases.getRepoById(repoId);
    return {
      id: repo.id,
      name: repo.name,
      group: {
        id: repo.group.id,
        name: repo.group.name,
        description: repo.group.description,
      },
    };
  }

  @ApiBearerAuth()
  @Post('repos')
  async createRepo(
    @AuthUserId() userId: EntityId,
    @Body() createRepoDto: CreateRepoDto,
  ): Promise<RepoResponseDto> {
    const repo = await this.repoUseCases.createRepoForUserId(
      userId,
      createRepoDto,
    );
    return {
      id: repo.id,
      name: repo.name,
      group: {
        id: repo.group.id,
        name: repo.group.name,
        description: repo.group.description,
      },
    };
  }

  @ApiBearerAuth()
  @Post('repos/:repoId/clean')
  async cleanRepo(
    @AuthUserId() userId: EntityId,
    @Param('repoId') repoId: EntityId,
  ): Promise<CleanRepoResponseDto> {
    return this.repoUseCases.cleanRepoForUserId(repoId, userId);
  }

  @ApiBearerAuth()
  @Put('repos/:repoId')
  async updateRepo(
    @Param('repoId') repoId: EntityId,
    @Body() updateRepoDto: UpdateRepoDto,
  ): Promise<RepoResponseDto> {
    const repo = await this.repoUseCases.updateRepo(repoId, updateRepoDto);
    return {
      id: repo.id,
      name: repo.name,
      group: {
        id: repo.group.id,
        name: repo.group.name,
        description: repo.group.description,
      },
    };
  }

  @ApiBearerAuth()
  @Delete('repos/:repoId')
  deleteRepo(
    @AuthUserId() userId: EntityId,
    @Param('repoId') repoId: EntityId,
  ) {
    return this.repoUseCases.deleteRepoForUserId(repoId, userId);
  }
}
