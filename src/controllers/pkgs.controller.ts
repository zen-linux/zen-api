import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { AuthUserId } from 'core/decorators/auth-user-id.decorator';
import {
  CreatePkgDto,
  PkgResponseDto,
  PkgsResponseDto,
} from 'core/dtos/pkg.dto';
import * as multer from 'multer';
import { Public } from 'use-cases/auth/decorators/public.decorator';
import { PkgUseCases } from 'use-cases/pkg/pkg-use-cases.service';

@ApiTags('Packages')
@Controller()
export class PkgsController {
  constructor(private pkgUseCases: PkgUseCases) {}

  @Public()
  @Get('pkgs')
  async getAll(): Promise<PkgsResponseDto> {
    const pkgs = await this.pkgUseCases.getAllPkgs();
    return {
      pkgs: pkgs.map((pkg) => ({
        id: pkg.id,
        name: pkg.name,
        description: pkg.desc,
        version: pkg.ver,
        release: pkg.rel,
        size: pkg.size,
        repo: {
          id: pkg.repo.id,
          name: pkg.repo.name,
          group: {
            id: pkg.repo.group.id,
            name: pkg.repo.group.name,
            description: pkg.repo.group.description,
          },
        },
      })),
    };
  }

  @Public()
  @Get('repos/:repoId/pkgs')
  async getAllForRepo(
    @Param('repoId') repoId: EntityId,
  ): Promise<PkgsResponseDto> {
    const pkgs = await this.pkgUseCases.getAllPkgsByRepoId(repoId);
    return {
      pkgs: pkgs.map((pkg) => ({
        id: pkg.id,
        name: pkg.name,
        description: pkg.desc,
        version: pkg.ver,
        release: pkg.rel,
        size: pkg.size,
        repo: {
          id: pkg.repo.id,
          name: pkg.repo.name,
          group: {
            id: pkg.repo.group.id,
            name: pkg.repo.group.name,
            description: pkg.repo.group.description,
          },
        },
      })),
    };
  }

  @Public()
  @Get('pkgs/:pkgId')
  async getById(@Param('pkgId') pkgId: EntityId) {
    return this.pkgUseCases.getPkgById(pkgId);
  }

  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @Post('pkgs')
  @UseInterceptors(
    FileInterceptor('pkg', {
      storage: multer.diskStorage({}),
      fileFilter: (_req, file, cb) => {
        cb(null, file.originalname.endsWith('.pkg.tar.gz'));
      },
    }),
  )
  async createPkg(
    @AuthUserId() userId: EntityId,
    @Body() createPkgDto: CreatePkgDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<PkgResponseDto> {
    const pkg = await this.pkgUseCases.createPkgForUserId(
      file,
      createPkgDto,
      userId,
    );
    return {
      id: pkg.id,
      name: pkg.name,
      description: pkg.desc,
      version: pkg.ver,
      release: pkg.rel,
      size: pkg.size,
      repo: {
        id: pkg.repo.id,
        name: pkg.repo.name,
        group: {
          id: pkg.repo.group.id,
          name: pkg.repo.group.name,
          description: pkg.repo.group.description,
        },
      },
    };
  }

  @ApiBearerAuth()
  @Delete('pkgs/:pkgId')
  deletePkg(@AuthUserId() userId: EntityId, @Param('pkgId') pkgId: EntityId) {
    return this.pkgUseCases.deletePkgForUserId(pkgId, userId);
  }
}
