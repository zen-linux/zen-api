import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthUserId } from 'core/decorators/auth-user-id.decorator';
import {
  ApiKeyResponseDto,
  ApiKeysResponseDto,
  CreateApiKeyDto,
  UpdateApiKeyDto,
} from 'core/dtos/api-key.dto';
import { ApiKeyUseCases } from 'use-cases/api-key/api-key-use-cases.service';

@ApiBearerAuth()
@ApiTags('Api Keys')
@Controller('api-keys')
export class ApiKeysController {
  constructor(private apiKeyUseCases: ApiKeyUseCases) {}

  @Get()
  async getAllForUserId(
    @AuthUserId() userId: EntityId,
  ): Promise<ApiKeysResponseDto> {
    const apiKeys = await this.apiKeyUseCases.getAllApiKeysForUserId(userId);
    return {
      apiKeys: apiKeys.map((apiKey) => ({
        id: apiKey.id,
        key: apiKey.key,
        description: apiKey.description,
      })),
    };
  }

  @Get(':groupId')
  async getById(
    @Param('apiKeyId') apiKeyId: EntityId,
  ): Promise<ApiKeyResponseDto> {
    const apiKey = await this.apiKeyUseCases.getApiKeyById(apiKeyId);
    if (apiKey) {
      return {
        id: apiKey.id,
        key: apiKey.key,
        description: apiKey.description,
      };
    }
    throw new NotFoundException();
  }

  @Post()
  async createApiKey(
    @AuthUserId() userId: EntityId,
    @Body() createApiKeyDto: CreateApiKeyDto,
  ): Promise<ApiKeyResponseDto> {
    const apiKey = await this.apiKeyUseCases.createApiKeyForUserId(
      userId,
      createApiKeyDto,
    );
    return {
      id: apiKey.id,
      key: apiKey.key,
      description: apiKey.description,
    };
  }

  @Put(':apiKeyId')
  async updateApiKey(
    @Param('apiKeyId') apiKeyId: EntityId,
    @Body() updateApiKeyDto: UpdateApiKeyDto,
    @AuthUserId() userId: EntityId,
  ): Promise<ApiKeyResponseDto> {
    const apiKey = await this.apiKeyUseCases.updateApiKeyForUserId(
      apiKeyId,
      updateApiKeyDto,
      userId,
    );
    return {
      id: apiKey.id,
      key: apiKey.key,
      description: apiKey.description,
    };
  }

  @Delete(':apiKeyId')
  deleteApiKey(
    @Param('apiKeyId') groupId: EntityId,
    @AuthUserId() userId: EntityId,
  ) {
    return this.apiKeyUseCases.deleteByIdForUserId(groupId, userId);
  }
}
