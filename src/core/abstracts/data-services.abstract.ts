import { ApiKeyRepository } from './repositories/api-key-repository';
import { GroupRepository } from './repositories/group-repository';
import { MemberRepository } from './repositories/member-repository';
import { PkgRepository } from './repositories/pkg-repository';
import { RepoRepository } from './repositories/repo-repository';
import { UserRepository } from './repositories/user-repository';

export abstract class DataServices {
  abstract apikeys: ApiKeyRepository;
  abstract groups: GroupRepository;
  abstract members: MemberRepository;
  abstract pkgs: PkgRepository;
  abstract repos: RepoRepository;
  abstract users: UserRepository;
}
