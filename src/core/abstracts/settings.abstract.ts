export abstract class Settings {
  abstract getDbConnectionString(): string;
  abstract getJwtSecret(): string;
  abstract getDataPath(): string;
}
