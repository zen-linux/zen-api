import { Pkg } from '../entities/pkg';
import { Repo } from '../entities/repo';

export abstract class Storage {
  abstract savePackage(pkg: Pkg, repo: Repo): Promise<boolean>;
}
