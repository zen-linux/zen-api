import { Member } from 'core/entities/member';
import { GenericRepository } from './generic-repository.abstract';

export abstract class MemberRepository implements GenericRepository<Member> {
  abstract getAll(): Promise<Member[]>;
  abstract get(id: EntityId): Promise<Member>;
  abstract create(item: Member): Promise<Member>;
  abstract update(id: EntityId, item: Member): Promise<Member>;
  abstract delete(id: EntityId): Promise<void>;
  abstract deleteAll(): Promise<void>;

  abstract getAllForGroupId(groupId: EntityId): Promise<Member[]>;
}
