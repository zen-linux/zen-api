export abstract class GenericRepository<T> {
  abstract getAll(): Promise<T[]>;

  abstract get(id: EntityId): Promise<T>;

  abstract create(item: T): Promise<T>;

  abstract update(id: EntityId, item: T): Promise<T>;

  abstract delete(id: EntityId): Promise<void>;

  abstract deleteAll(): Promise<void>;
}
