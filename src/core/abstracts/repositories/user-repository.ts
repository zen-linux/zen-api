import { User } from '../../entities/user';
import { GenericRepository } from './generic-repository.abstract';

export abstract class UserRepository implements GenericRepository<User> {
  abstract getAll(): Promise<User[]>;
  abstract get(id: EntityId): Promise<User>;
  abstract create(item: User): Promise<User>;
  abstract update(id: EntityId, item: User): Promise<User>;
  abstract delete(id: EntityId): Promise<void>;
  abstract deleteAll(): Promise<void>;

  abstract getByEmail(email: string): Promise<User>;
}
