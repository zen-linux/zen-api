import { Group } from '../../entities/group';
import { GenericRepository } from './generic-repository.abstract';

export abstract class GroupRepository implements GenericRepository<Group> {
  abstract getAll(): Promise<Group[]>;
  abstract get(id: EntityId): Promise<Group>;
  abstract create(item: Group): Promise<Group>;
  abstract update(id: EntityId, item: Group): Promise<Group>;
  abstract delete(id: EntityId): Promise<void>;
  abstract deleteAll(): Promise<void>;

  abstract getByIdForUserId(
    groupId: EntityId,
    userId: EntityId,
  ): Promise<Group>;
  abstract getByName(name: string): Promise<Group>;
}
