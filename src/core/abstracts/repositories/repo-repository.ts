import { Repo } from 'core/entities/repo';
import { GenericRepository } from './generic-repository.abstract';

export abstract class RepoRepository implements GenericRepository<Repo> {
  abstract getAll(): Promise<Repo[]>;
  abstract get(id: EntityId): Promise<Repo>;
  abstract create(item: Repo): Promise<Repo>;
  abstract update(id: EntityId, item: Repo): Promise<Repo>;
  abstract delete(id: EntityId): Promise<void>;
  abstract deleteAll(): Promise<void>;

  abstract getByIdForUserId(repoId: EntityId, userId: EntityId): Promise<Repo>;
  abstract getAllForUserId(userId: EntityId): Promise<Repo[]>;
  abstract getAllByGroupId(groupId: EntityId): Promise<Repo[]>;
}
