import { ApiKey } from 'core/entities/api-key';
import { GenericRepository } from './generic-repository.abstract';

export abstract class ApiKeyRepository implements GenericRepository<ApiKey> {
  abstract getAll(): Promise<ApiKey[]>;
  abstract get(id: EntityId): Promise<ApiKey>;
  abstract create(item: ApiKey): Promise<ApiKey>;
  abstract update(id: EntityId, item: ApiKey): Promise<ApiKey>;
  abstract delete(id: EntityId): Promise<void>;
  abstract deleteAll(): Promise<void>;

  abstract getAllByUserId(userId: EntityId): Promise<ApiKey[]>;
  abstract getByKey(key: string): Promise<ApiKey>;
  abstract deleteByIdForUserId(id: EntityId, userId: EntityId): Promise<void>;
  abstract deleteAllForUserId(userId: EntityId): Promise<void>;
}
