import { Pkg } from 'core/entities/pkg';
import { GenericRepository } from './generic-repository.abstract';

export abstract class PkgRepository implements GenericRepository<Pkg> {
  abstract getAll(): Promise<Pkg[]>;
  abstract get(id: EntityId): Promise<Pkg>;
  abstract create(item: Pkg): Promise<Pkg>;
  abstract update(id: EntityId, item: Pkg): Promise<Pkg>;
  abstract delete(id: EntityId): Promise<void>;
  abstract deleteAll(): Promise<void>;

  abstract getAllByRepoId(repoId: EntityId): Promise<Pkg[]>;
  abstract deleteForUserId(id: EntityId, userId: EntityId): Promise<void>;
}
