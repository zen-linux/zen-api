import { PartialType } from '@nestjs/swagger';
import {
  IsAlphanumeric,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
  Validate,
} from 'class-validator';
import { UniqueGroupValidator } from 'use-cases/group/validators/unique-group.validator';

export class CreateGroupDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(16)
  @MinLength(3)
  @IsAlphanumeric()
  @Validate(UniqueGroupValidator)
  name!: string;

  @IsString()
  @IsOptional()
  description?: string;
}

export class UpdateGroupDto extends PartialType(CreateGroupDto) {}

export class GroupResponseDto {
  id!: EntityId;
  name!: string;
  description!: string;
}

export class GroupsResponseDto {
  groups!: GroupResponseDto[];
}
