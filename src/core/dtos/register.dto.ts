import {
  IsAlphanumeric,
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
  Validate,
} from 'class-validator';
import { UniqueEmailValidator } from 'use-cases/auth/validators/unique-email.validator';

export class RegisterDto {
  @IsEmail()
  @IsNotEmpty()
  @Validate(UniqueEmailValidator)
  email!: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(16)
  @MinLength(3)
  @IsAlphanumeric()
  username!: string;

  @IsString()
  @IsNotEmpty()
  password!: string;
}

export class RegisterResponseDto {}
