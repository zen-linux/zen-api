import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { RepoResponseDto } from './repo.dto';

export class CreatePkgDto {
  @IsString()
  @IsNotEmpty()
  repoId!: string;

  @ApiProperty({
    type: 'string',
    format: 'binary',
    required: true,
  })
  pkg!: Express.Multer.File;
}

export class RemovePkgDto {
  //@IsIn(repoNames)
  repo!: string;

  @IsNotEmpty()
  pkg!: string;
}

export class PkgResponseDto {
  id!: EntityId;
  name!: string;
  description!: string;
  version!: string;
  release!: number;
  size!: number;
  repo!: RepoResponseDto;
}

export class PkgsResponseDto {
  pkgs!: PkgResponseDto[];
}
