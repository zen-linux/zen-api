import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';
import { GroupResponseDto } from './group.dto';
import { UserResponseDto } from './user.dto';

export class CreateMemberDto {
  @IsNotEmpty()
  @IsString()
  groupId!: string;

  @IsNotEmpty()
  @IsString()
  userId!: string;

  @IsBoolean()
  @IsNotEmpty()
  isAdmin!: boolean;
}

export class UpdateMemberDto {
  @IsBoolean()
  @IsNotEmpty()
  isAdmin!: boolean;
}

export class MemberResponseDto {
  id!: EntityId;
  isAdmin!: boolean;
  user!: UserResponseDto;
  group!: GroupResponseDto;
}

export class MembersResponseDto {
  members!: MemberResponseDto[];
}
