import { IsNotEmpty, IsString } from 'class-validator';

export class CreateApiKeyDto {
  @IsString()
  @IsNotEmpty()
  description!: string;
}

export class UpdateApiKeyDto {
  @IsString()
  @IsNotEmpty()
  description!: string;
}

export class ApiKeyResponseDto {
  id!: EntityId;
  key!: string;
  description!: string;
}

export class ApiKeysResponseDto {
  apiKeys!: ApiKeyResponseDto[];
}
