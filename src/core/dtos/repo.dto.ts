import { IsNotEmpty, IsString } from 'class-validator';
import { GroupResponseDto } from './group.dto';

export class CreateRepoDto {
  @IsString()
  @IsNotEmpty()
  name!: string;

  @IsString()
  @IsNotEmpty()
  groupId!: string;
}

export class UpdateRepoDto {
  @IsString()
  @IsNotEmpty()
  name?: string;
}

export class RepoResponseDto {
  id!: EntityId;
  name!: string;
  group!: GroupResponseDto;
}

export class ReposResponseDto {
  repos!: RepoResponseDto[];
}

export class CleanRepoResponseDto {
  savedFiles!: string[];
  deletedFiles!: string[];
}
