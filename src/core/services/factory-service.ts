import { Injectable } from '@nestjs/common';
import { DataServices } from '../abstracts/data-services.abstract';

@Injectable()
export abstract class FactoryService<T> {
  constructor(protected dataServices: DataServices) {}

  abstract create(createDto: object): Promise<T>;
  abstract update(id: EntityId, updateDto: object): Promise<T>;
}
