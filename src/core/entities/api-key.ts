import { Entity } from './entity';
import { User } from './user';

export class ApiKey extends Entity {
  key: string;
  description: string;
  user: User;

  constructor(data: PartialBy<ApiKey, 'id'>) {
    super(data.id);

    this.key = data.key;
    this.description = data.description;
    this.user = data.user;
  }
}
