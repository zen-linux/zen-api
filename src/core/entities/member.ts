import { Entity } from './entity';
import { Group } from './group';
import { User } from './user';

export class Member extends Entity {
  user: User;
  group: Group;
  isAdmin: boolean;

  constructor(data: PartialBy<Member, 'id'>) {
    super(data.id);

    this.isAdmin = data.isAdmin;
    this.group = data.group;
    this.user = data.user;
  }
}
