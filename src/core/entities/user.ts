import { Entity } from './entity';

export class User extends Entity {
  name: string;
  email: string;
  password: string;

  constructor(data: PartialBy<User, 'id'>) {
    super(data.id);

    this.name = data.name;
    this.email = data.email;
    this.password = data.password;
  }
}
