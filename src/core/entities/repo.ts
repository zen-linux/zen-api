import { Entity } from './entity';
import { Group } from './group';

export class Repo extends Entity {
  name: string;
  group: Group;

  constructor(data: PartialBy<Repo, 'id'>) {
    super(data.id);

    this.name = data.name;
    this.group = data.group;
  }
}
