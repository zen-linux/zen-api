import { Entity } from './entity';
import { Repo } from './repo';

export class Pkg extends Entity {
  name: string;
  desc: string;
  ver: string;
  rel: number;
  size: number;
  repo: Repo;

  constructor(data: PartialBy<Pkg, 'id'>) {
    super(data.id);

    this.name = data.name;
    this.desc = data.desc;
    this.ver = data.ver;
    this.rel = data.rel;
    this.size = data.size;
    this.repo = data.repo;
  }
}
