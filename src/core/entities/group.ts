import { Entity } from './entity';

export class Group extends Entity {
  name: string;
  description: string;

  constructor(data: PartialBy<Group, 'id'>) {
    super(data.id);

    this.name = data.name;
    this.description = data.description;
  }
}
