import { v4 as uuid } from 'uuid';

export class Entity {
  id: EntityId;

  constructor(id?: EntityId) {
    this.id = id !== undefined ? id : uuid();
  }
}
