import { ServeFaviconMiddleware } from '@nest-middlewares/serve-favicon';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { join } from 'path';
import { ApiKeysController } from './controllers/api-keys.controller';
import { AuthController } from './controllers/auth.controller';
import { GroupsController } from './controllers/groups.controller';
import { MembersController } from './controllers/members.controller';
import { PacmanController } from './controllers/pacman.controller';
import { PkgsController } from './controllers/pkgs.controller';
import { ReposController } from './controllers/repos.controller';
import { UsersController } from './controllers/users.controller';
import { RequestLoggerMiddleware } from './core/middlewares/request-logger.middleware';
import { ApiKeyUseCasesModule } from './use-cases/api-key/api-key-use-cases.module';
import { AuthUseCasesModule } from './use-cases/auth/auth-use-cases.module';
import { GroupUseCasesModule } from './use-cases/group/group-use-cases.module';
import { MemberUseCasesModule } from './use-cases/member/member-use-cases.module';
import { PkgUseCasesModule } from './use-cases/pkg/pkg-use-cases.module';
import { RepoUseCasesModule } from './use-cases/repo/repo-use-cases.module';
import { UserUseCasesModule } from './use-cases/user/user-use-cases.module';

@Module({
  imports: [
    ApiKeyUseCasesModule,
    AuthUseCasesModule,
    GroupUseCasesModule,
    MemberUseCasesModule,
    PkgUseCasesModule,
    RepoUseCasesModule,
    UserUseCasesModule,
  ],
  controllers: [
    ApiKeysController,
    AuthController,
    GroupsController,
    MembersController,
    PkgsController,
    ReposController,
    UsersController,
    PacmanController, // this needs to be at end
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestLoggerMiddleware).forRoutes('*');

    ServeFaviconMiddleware.configure(
      join(__dirname, '..', 'public', 'favicon.ico'),
    );
    consumer.apply(ServeFaviconMiddleware).forRoutes('favicon.ico');
  }
}
